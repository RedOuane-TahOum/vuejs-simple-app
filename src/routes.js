import languagesComponent from './components/languages.vue'
import homeComponent from './components/home.vue'
import usersComponent from './components/users.vue'

export default [
  {path: '/', component: homeComponent},
  {path: '/languages', component : languagesComponent},
  {path: '/users', component : usersComponent}
]
